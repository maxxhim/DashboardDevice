# DashboardDevice

This application display the stock market value fluctuations in graphical form from the api https://exchangeratesapi.io/

The application is available online at http://maxxhim.pythonanywhere.com/

You can change the "days" in the url and "currencies" like :


>http://maxxhim.pythonanywhere.com/days=30&currencies=USD,CHF
>
> http://maxxhim.pythonanywhere.com/days=30&currencies=CAD
>
>http://maxxhim.pythonanywhere.com/days=50&currencies=CHF
>
>http://maxxhim.pythonanywhere.com/days=180&currencies=CHF

